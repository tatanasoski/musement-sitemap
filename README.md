
# Musement Sitemap

Tool which generates sitemap from the Musement API using Musement Sdk and uploading on Amazon S3 bucket

## Getting Started

You will have to have permission for accessing this tool from the repository

### Installing

Adding it to your composer file

```
"repositories": [
{
    "type": "package",
    "package": {
        "name": "tatanasoski/musement-sitemap",
        "version": "0.0.10",
        "source": {
            "url": "git@bitbucket.org:tatanasoski/musement-sitemap.git",
            "type": "git",
            "reference": "master"
        },
        "autoload": {
            "psr-4" : {
                "MusementSitemap\\" : "src/"
            }
        }
    }
}]
```

And add in the required

```
"require": {
    "tatanasoski/musement-sitemap": "^0.0.10"
}
```

### Code Example

```
require __DIR__ . '/vendor/autoload.php';

$musementSitemap = new MusementSitemap\Sitemap([
    'aws_credentials' => '{"version":"latest","region":"eu-west-1","credentials":{"key":"{KEY_HERE}","secret":"{SECRET_HERE}"}}',
    'upload_location' => '{BUCKET-NAME}',
    'temp_folder'     => '/tmp',
    'sitemap'         => [
        'cities' => [
            'limit' => 20,
            'importance' => 0.7
        ],
        'activities' => [
            'limit' => 20,
            'importance' => 0.5
        ]
    ]
]);
$musementSitemap->generate('tatanasoski');
```
