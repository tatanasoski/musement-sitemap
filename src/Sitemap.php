<?php
namespace MusementSitemap;

use \DI\ContainerBuilder;

use MusementSdk\MusementSdk;

use MusementSitemap\Adapters\WriterAdapter;
use MusementSitemap\Adapters\GeneratorAdapter;
use MusementSitemap\Adapters\DataProviderAdapter;
use MusementSitemap\Adapters\FileAdapter;

use MusementSitemap\Services\SitemapGeneratorService;

use MusementSitemap\Libraries\Config;

use \Aws\Sdk;

use \Psr\Container\ContainerInterface;

/**
 * Class for generating sitemap
 */
class Sitemap {
    
    private $config;


    private $diContainer;
    
    /**
     * @var Interfaces\FileInterface 
     */
    private $fileManager;
    
    /**
     * @var Interfaces\DataProviderInterface 
     */
    private $dataProvider;
    
    /**
     * @var Interfaces\GeneratorInterface 
     */
    private $sitemapGenerator;
    
    /**
     * Constructor function
     */
    public function __construct($config = null) {
        $this->setConfig($config);
        
        $this->diContainer          = $this->getDiContainer();
        $this->fileManager          = $this->getDiContainer()->get('FileManager');
        $this->dataProvider         = $this->getDiContainer()->get('DataProvider');
        $this->sitemapGenerator     = $this->getDiContainer()->get('Generator');
    }
    
    /**
     * Main function for generating the sitemap
     * @param string $filePrefix String to be prepended on the begining of the file
     */
    public function generate($filePrefix){
        $this->sitemapGenerator->setPrefix($filePrefix);
        $this->sitemapGenerator->setSeparator('_');
        $this->generateLocales();
    }
    
    /**
     * Function for geting the config object
     * @return Config|null
     */
    public function getConfig(){
        return $this->config;
    }
    
    /**
     * Function for setting config
     * @param type $config
     */
    public function setConfig($config){
        $this->config = new Config($config);
    }
    
    /**
     * Function for getting dipendency injection container
     * @return \Psr\Container;
     */
    private function getDiContainer(){
        if ($this->diContainer) {
            return $this->diContainer;
        }
        $builder = new ContainerBuilder();
        $builder->addDefinitions($this->getDefinitions());
        return $builder->build();
    }
    
    /**
     * Function for returning dependency injection definitions
     * @return type
     */
    private function getDefinitions(){
        return [            
            'DataProviderService' => new MusementSdk(),
            'DataProvider' => function(ContainerInterface $c){
                return new DataProviderAdapter($c->get('DataProviderService'));
            },
            'FileService' => function(){
                return (new Sdk(json_decode($this->config->get('aws_credentials'), true)))->createS3();
            },
            'FileManager' => function(ContainerInterface $c){
                return new FileAdapter(
                    $c->get('FileService'), 
                    $this->config->get('upload_location')
                );
            },
            'WriterService' => function(){
                return new \XMLWriter();
            },
            'Writer' => function(ContainerInterface $c){
                return new WriterAdapter($c->get('WriterService'));
            },
            'GeneratorService' => function(ContainerInterface $c){
                return new SitemapGeneratorService($c->get('Writer'));
            },
            'Generator' => function(ContainerInterface $c){
                return new GeneratorAdapter($c->get('GeneratorService'));
            },
        ];
    }
    
    /**
     * Function for looping locales
     */
    private function generateLocales(){
        foreach ($this->dataProvider->getAllLocales() as $locale) {
            $this->sitemapGenerator->setPath($this->getConfig()->get('temp_folder'));
            $this->sitemapGenerator->start($locale->sTag);
            $this->generateCities($locale);
            $this->sitemapGenerator->finish();
            $this->fileManager->upload(
                $this->sitemapGenerator->getFullPath(),
                $this->sitemapGenerator->getFile()
            );
            @unlink($this->sitemapGenerator->getFullPath());
        } 
    }
    
    /**
     *  Function for looping thorugh cities
     * @param \MusementSdk\Entities\Locale $locale
     */
    private function generateCities($locale){
        $citiesConfig = $this->getConfig()->get('sitemap')->get('cities');
        foreach ($this->dataProvider->getCitiesByLocale($locale->sTag, 1, $citiesConfig->get('limit')) as $city) {
            $this->sitemapGenerator->addItem($city->sUrl,$citiesConfig->get('importance'));
            $this->generateActivities($locale, $city);
        }           
    }
    
    /**
     * Function for looping through activities
     * @param \MusementSdk\Entities\Locale $locale
     * @param \MusementSdk\Entities\City $city
     */
    private function generateActivities($locale, $city){
        $activitiesConfig = $this->getConfig()->get('sitemap')->get('activities');
        foreach ($this->dataProvider->getActivitiesByCity($city->iId, $locale->sTag, 1, $activitiesConfig->get('limit')) as $activity) {
            $this->sitemapGenerator->addItem($activity->sUrl, $activitiesConfig->get('importance'));
        }
    }
}
