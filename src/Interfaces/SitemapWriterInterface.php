<?php
namespace MusementSitemap\Interfaces;
interface SitemapWriterInterface {
    public function startDocument($version, $encoding);
    public function endDocument();
    public function startElement($name);
    public function endElement();
    public function writeElement($name, $content = null);
    public function writeAttribute($name, $content = null);
}
