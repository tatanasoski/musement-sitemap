<?php
namespace MusementSitemap\Interfaces;
interface GeneratorInterface {
    public function setPath($path);
    public function setPrefix($prefix);
    public function setSeparator($separator);
    public function start($locale);
    public function addItem($url, $priority, $frequency);
    public function finish();
}
