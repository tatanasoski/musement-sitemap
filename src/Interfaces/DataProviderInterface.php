<?php
namespace MusementSitemap\Interfaces;
interface DataProviderInterface {
    public function getAllLocales();
    public function getCitiesByLocale($locale, $start, $limit);
    public function getActivitiesByCity($city, $start, $limit);
}
