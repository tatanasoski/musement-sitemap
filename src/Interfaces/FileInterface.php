<?php
namespace MusementSitemap\Interfaces;

interface FileInterface {
    public function upload($source, $destination);
    public function delete($source);
}
