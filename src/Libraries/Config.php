<?php
namespace MusementSitemap\Libraries;
/**
 * Config class for storing configs
 */
class Config {
    
    private $config;
    
    public function __construct($config = []) {
        $this->config = $config;
    }
    
    /**
     * Returns config value if is array, returns new config object
     * @param string $key
     * @return mixed
     */
    public function get($key){
        return isset($this->config[$key]) 
        ? (
            is_array($this->config[$key]) 
            ? new self($this->config[$key]) 
            : $this->config[$key])  
        : null;
    }
    
    /**
     * Function for setting config key
     * @param string $key
     * @param mixed $value
     */
    public function set($key, $value) {
        $this->config[$key] = $value;
    }
    
    /**
     * Function for geting whole config array
     * @return type
     */
    public function getAll(){
        return $this->config;
    }
}
