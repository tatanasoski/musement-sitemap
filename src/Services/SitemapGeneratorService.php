<?php

namespace MusementSitemap\Services;

class SitemapGeneratorService {

	private $writer;

	const SCHEMA = 'http://www.sitemaps.org/schemas/sitemap/0.9';
	const DEFAULT_PRIORITY = 0.1;

	public function __construct($writer) {
	    $this->writer = $writer;
	}
        
	public function startSitemap($fullFilename) {            
            $this->writer->init($fullFilename);
            $this->writer->startDocument('1.0', 'UTF-8');
            $this->writer->startElement('urlset');
            $this->writer->writeAttribute('xmlns', self::SCHEMA);
	}

	public function addItem($url, $priority = self::DEFAULT_PRIORITY, $changefreq = null, $lastmod = null) {
		
            $this->writer->startElement('url');
            $this->writer->writeElement('loc', $url);
            $this->writer->writeElement('priority', $priority);
            
            if ($changefreq !== null) {
                $this->writer->writeElement('changefreq', $changefreq);
            }

            if ($lastmod !== null) {
                $this->writer->writeElement('lastmod', $lastmod);
            }

            $this->writer->endElement();
	}
        
	public function endSitemap() {
            $this->writer->endElement();
            $this->writer->endDocument();
	}
}