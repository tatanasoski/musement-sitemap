<?php
namespace MusementSitemap\Adapters;

use MusementSitemap\Interfaces\SitemapWriterInterface;

/**
 * Class for writing xml
 */
class WriterAdapter implements SitemapWriterInterface{
    
    private $service;
    public function __construct($service) {
        $this->service = $service;
    }
    
    /**
     * Function for initializing the class
     * @param type $filepath
     * @throws \Exception
     */
    public function init($filepath){
        if (is_file($filepath)) {
            @unlink($filepath);
        }
        
        if (!is_dir(dirname($filepath))) {
            throw new \Exception('Filepath is not directory');
        }
        
        $this->service->openUri($filepath);
        $this->service->setIndent(true);
    }
    
    /**
     * Function for ending xml doclument
     */
    public function endDocument() {
        $this->service->endDocument();
    }

    /**
     * Function for ending the last xml element
     */
    public function endElement() {
        $this->service->endElement();
    }

    /**
     * Function for starting xml document
     * @param string $version
     * @param string $encoding
     */
    public function startDocument($version, $encoding) {
        $this->service->startDocument($version, $encoding);
    }

    /**
     * Function for starting xml element
     * @param string $name
     */
    public function startElement($name) {
        $this->service->startElement($name);
    }
    
    /**
     * Function for writing xml element
     * @param string $name
     * @param string $content
     */
    public function writeElement($name, $content = null) {
        $this->service->writeElement($name, $content);
    }
    
    /**
     * Function for writing xml attribute
     * @param string $name
     * @param string $content
     */
    public function writeAttribute($name, $content = null) {
        $this->service->writeAttribute($name, $content);
    }
}
