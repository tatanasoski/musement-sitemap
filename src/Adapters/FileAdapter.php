<?php
namespace MusementSitemap\Adapters;
use MusementSitemap\Interfaces\FileInterface;
class FileAdapter implements FileInterface{
    private $client;
    
    private $bucket;
    
    public function __construct($client, $bucket) {
        $this->client = $client;
        $this->bucket = $bucket;
    }

    /**
     * Function for uploading
     * @param string $source
     * @param string $destination
     * @return type
     */
    public function upload($source, $destination) {
        return $this->client->putObject([
            'Bucket'     => $this->getBucket(),
            'Key'        => $destination,
            'SourceFile' => $source
        ]);
    }

    /**
     * Function for deleting
     * @param type $source
     * @return type
     */
    public function delete($source) {
        return $this->client->deleteObject(array(
                'Bucket' => $this->getBucket(),
                'Key' => $source
            ));
    }
    
    /**
     * Function for returning bucket
     * @return string
     */
    private function getBucket(){
        return $this->bucket;
    }
}
