<?php
namespace MusementSitemap\Adapters;
use MusementSitemap\Interfaces\DataProviderInterface;
/**
 * Data Provider Adapter
 */
class DataProviderAdapter implements DataProviderInterface {
    private $service;
    
    public function __construct($service) {
        $this->service = $service;
    }
    
    /**
     * Function for returning all locales
     * @return array
     */
    public function getAllLocales() {
        return $this->service->getLocales()->getAll();
    }
    
    /**
     * Function for returning all activities by given locale and city
     * @param int $cityId
     * @param string $locale
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function getActivitiesByCity($cityId, $locale, $page=1, $limit=100) {
        $filter     = $this->service->getFilter();
        $filter->set('Accept-Language', $locale);
        $pagination = $this->service->getPagination();
        $pagination->setPage($page);
        $pagination->setLimit($limit);
        return $this->service->getActivities()->getAllByCity($cityId, $filter, $pagination);
    }

    /**
     * Function for returning all cities by given locale
     * @param string $locale
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function getCitiesByLocale($locale, $page=1, $limit=100) {
        $filter     = $this->service->getFilter();
        $filter->set('Accept-Language', $locale);
        $pagination = $this->service->getPagination();
        $pagination->setPage($page);
        $pagination->setLimit($limit);
        return $this->service->getCities()->getAllBy($filter, $pagination);
    }

}
