<?php
namespace MusementSitemap\Adapters;
use MusementSitemap\Interfaces\GeneratorInterface;
/**
 * Class for generating sitemap
 */
class GeneratorAdapter implements GeneratorInterface {
    
    private $path;
    private $prefix = null;
    private $separator = null;
    private $service;
    private $locale;
    
    public function __construct($service) {
        $this->service = $service;
    }
    
    /**
     * Function for adding sitemap item
     * @param string $url
     * @param float $priority
     * @param string $frequency
     */
    public function addItem($url, $priority, $frequency = null) {
        $this->service->addItem($url, $priority, $frequency);
    }

    /**
     * Function for ending sitemap
     */
    public function finish() {
        $this->service->endSitemap();
    }

    /**
     * Function for setting path where the sitemap is going to be generated
     * @param string $path
     */
    public function setPath($path) {
        $this->path = rtrim($path, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;
    }

    /**
     * Function for setting prefix of the sitemap
     * @param string $prefix
     */
    public function setPrefix($prefix) {
        $this->prefix = $prefix;        
    }
    
    /**
     * Function for setting separator of the sitemap
     * @param string $separator
     */
    public function setSeparator($separator) {
        $this->separator = $separator;        
    }
    
    /**
     * Function for setting locale of the sitemap
     * @param type $locale
     */
    public function setLocale($locale) {
        $this->locale = $locale;        
    }

    /**
     * Function for staring the sitemap
     * @param type $locale
     */
    public function start($locale) {
        if($this->path == null) {
            throw new \Exception('Sitemap path missing');
        }
        
        $this->setLocale($locale);
        
        $this->service->startSitemap($this->getFullPath());
    }
    
    /**
     * Function for getting full path of the sitemap
     * @return string
     */
    public function getFullPath(){
        return $this->path.$this->getFile();
    }
    
    /**
     * Function for getting the filename of the sitemap
     * @return string
     */
    public function getFile(){
        return ($this->prefix ?: '') . ($this->separator ?: '') . ($this->locale.'.xml');
    }

}